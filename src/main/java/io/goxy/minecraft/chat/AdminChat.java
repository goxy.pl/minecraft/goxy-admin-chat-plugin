package io.goxy.minecraft.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;
import pl.goxy.minecraft.pubsub.PubSub;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdminChat implements Listener {
    public static final String SEND_PERMISSION = "goxy.admin-chat.send";
    public static final String RECEIVE_PERMISSION = "goxy.admin-chat.receive";
    public static final String PUB_SUB_HANDLER = "admin-chat";
    private static final Pattern hexPattern = Pattern.compile("&#([A-Fa-f0-9]{6})");

    private final AdminChatConfiguration configuration;
    private final Plugin plugin;
    private final LuckPermsSupport luckPermsSupport;
    private final PubSub pubSub;

    public AdminChat(AdminChatConfiguration configuration, Plugin plugin, LuckPermsSupport luckPermsSupport, PubSub pubSub) {
        this.configuration = configuration;
        this.plugin = plugin;
        this.luckPermsSupport = luckPermsSupport;
        this.pubSub = pubSub;
    }

    @EventHandler(ignoreCancelled = true)
    public void playerMessage(AsyncPlayerChatEvent event) {
        Logger logger = this.plugin.getLogger();
        String message = event.getMessage();
        Player player = event.getPlayer();
        String prefix = this.configuration.getPrefix();

        if (!message.startsWith(prefix) || !player.hasPermission(SEND_PERMISSION)) {
            return;
        }
        event.setCancelled(true);

        message = message.substring(prefix.length());

        String name = player.getName();
        logger.info(name + " sent message to admin chat: " + message);

        if (this.luckPermsSupport != null) {
            String usernameColor = this.luckPermsSupport.getColor(player, "username-color");
            if (usernameColor != null) {
                name = this.colorize(usernameColor) + name;
            }
        }

        AdminMessage adminMessage = new AdminMessage(name, message);
        if (this.pubSub != null) {
            this.pubSub.sendPluginNetwork(PUB_SUB_HANDLER, adminMessage).whenComplete((x, throwable) ->
            {
                if (throwable != null) {
                    logger.log(Level.SEVERE, "Failed to send admin chat message to pubsub", throwable);
                }
            });
        } else {
            this.handleMessage(adminMessage, null);
        }
    }

    public void handleMessage(AdminMessage data, String serverName) {
        String name = data.getName();
        String message = data.getMessage();

        String format = serverName != null ? this.configuration.getServerFormat() : this.configuration.getDefaultFormat();
        String messageContent = this.colorize(format).replace("{player_name}", name).replace("{message}", message.trim()).replace("{server_name}", serverName != null ? serverName : "null");

        this.plugin.getServer()
                .getScheduler()
                .runTask(this.plugin,
                        () -> this.plugin.getServer().broadcast(messageContent, RECEIVE_PERMISSION));
    }

    private String colorize(String message) {
        return this.translateHexColorCodes(ChatColor.translateAlternateColorCodes('&', message));
    }

    private String translateHexColorCodes(String message) {
        Matcher matcher = hexPattern.matcher(message);
        StringBuffer buffer = new StringBuffer(message.length() + 32);

        while (matcher.find()) {
            String group = matcher.group(1);
            matcher.appendReplacement(buffer, ChatColor.COLOR_CHAR + "x" + ChatColor.COLOR_CHAR + group.charAt(
                    0) + ChatColor.COLOR_CHAR + group.charAt(1) + ChatColor.COLOR_CHAR + group.charAt(
                    2) + ChatColor.COLOR_CHAR + group.charAt(3) + ChatColor.COLOR_CHAR + group.charAt(
                    4) + ChatColor.COLOR_CHAR + group.charAt(5));
        }
        return matcher.appendTail(buffer).toString();
    }
}
