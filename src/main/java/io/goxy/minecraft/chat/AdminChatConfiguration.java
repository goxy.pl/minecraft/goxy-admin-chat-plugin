package io.goxy.minecraft.chat;

public class AdminChatConfiguration {
    private String prefix;

    private String defaultFormat;
    private String serverFormat;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDefaultFormat() {
        return defaultFormat;
    }

    public void setDefaultFormat(String defaultFormat) {
        this.defaultFormat = defaultFormat;
    }

    public String getServerFormat() {
        return serverFormat;
    }

    public void setServerFormat(String serverFormat) {
        this.serverFormat = serverFormat;
    }
}
