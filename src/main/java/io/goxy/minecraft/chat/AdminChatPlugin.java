package io.goxy.minecraft.chat;

import net.luckperms.api.LuckPerms;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import pl.goxy.minecraft.pubsub.PubSub;
import pl.goxy.minecraft.pubsub.PubSubService;

import java.util.Objects;

public class AdminChatPlugin extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        this.saveDefaultConfig();

        PluginManager pluginManager = this.getServer().getPluginManager();

        LuckPermsSupport luckPermsSupport;
        if (pluginManager.isPluginEnabled("LuckPerms")) {
            RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager()
                    .getRegistration(LuckPerms.class);
            luckPermsSupport = new LuckPermsSupport(Objects.requireNonNull(provider).getProvider());
        } else {
            luckPermsSupport = null;
        }

        PubSub pubSub;
        if (pluginManager.isPluginEnabled("goxy-pubsub")) {
            PubSubService pubSubService = (PubSubService) Objects.requireNonNull(
                    pluginManager.getPlugin("goxy-pubsub"));
            pubSub = pubSubService.getPubSub(this);
        } else {
            pubSub = null;
        }

        AdminChatConfiguration configuration = new AdminChatConfiguration();
        configuration.setPrefix(this.getConfig().getString("admin-chat.prefix"));
        configuration.setDefaultFormat(this.getConfig().getString("format.default"));
        configuration.setServerFormat(this.getConfig().getString("format.server"));

        AdminChat adminChat = new AdminChat(configuration, this, luckPermsSupport, pubSub);
        pluginManager.registerEvents(adminChat, this);

        if (pubSub != null) {
            pubSub.registerHandler(AdminChat.PUB_SUB_HANDLER, AdminMessage.class, (context, data) ->
            {
                String serverName = context.getServer() != null ? context.getServer().getName() : null;
                adminChat.handleMessage(data, serverName);
            }).async(true);
        }
    }
}
