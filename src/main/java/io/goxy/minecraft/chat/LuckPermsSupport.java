package io.goxy.minecraft.chat;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.cacheddata.CachedDataManager;
import net.luckperms.api.cacheddata.CachedMetaData;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import org.bukkit.entity.Player;

public class LuckPermsSupport
{
    private final LuckPerms luckPerms;

    public LuckPermsSupport(LuckPerms luckPerms)
    {
        this.luckPerms = luckPerms;
    }

    public String getColor(Player player, String key)
    {
        CachedMetaData metaData = this.getGroupMetaData(player);
        return metaData != null ? metaData.getMetaValue(key) : null;
    }

    private CachedMetaData getGroupMetaData(Player player)
    {
        CachedDataManager dataManager = this.getCachedDataManager(player);
        return dataManager != null ? dataManager.getMetaData(
                this.luckPerms.getContextManager().getStaticQueryOptions()) : null;
    }

    private CachedDataManager getCachedDataManager(Player player)
    {
        User user = this.luckPerms.getUserManager().getUser(player.getUniqueId());
        return user != null ? user.getCachedData() : null;
    }
}
